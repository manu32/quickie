/**
 * Created by shahab on 10/7/15.
 */
module.exports = {
    CustomerService : require('./CustomerService'),
    AdminService : require('./AdminService'),
    AppVersionService : require('./AppVersionService')
};