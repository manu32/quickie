# README #

Quickie 

### repository Info ###

* Quickie Nodejs Backend
* Version : 0.0.1


### set up Quickie  ###

* Git
* NodeJs with NPM
* MongoDB

### Run Quickie  ###

* Clone the repo with git command "git clone git@bitbucket.org:manu32/quickie.git"
* cd quickie
* npm install / sudo npm install (if permission error)
* node server.js
* open http://localhost:8000 in the browser to see the default page
* open http://localhost:8000/documentation to see the API documentation

### Contribution guidelines ###

* Airbnb JavaScript Style Guide() { link: https://github.com/airbnb/javascript }
* Code review

