/**
 * Created by shahab on 10/7/15.
 */
'use strict';
var CustomerRoute = require('./CustomerRoute');
var AdminRoute = require('./AdminRoute');

var all = [].concat(CustomerRoute,AdminRoute);

module.exports = all;

